const express = require('express');
const bodyParser=require('body-parser');
const swaggerUi = require("swagger-ui-express");
const swaggerJsdoc = require("swagger-jsdoc");
const Joi = require('joi');
const router      = express.Router();

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

app.get('/',(req,res)=>
{
res.send("welcome to this page");
});

app.post('/signup', (req, res, next) => {

    const data = req.body;

    const schema = Joi.object().keys({

        name: Joi.string().min(6).max(24).required(),
      email: Joi.string().email().required(),
      password: Joi.string().min(8).max(30).regex(/[a-zA-Z0-9]{3,30}/).required(),
      _id: Joi.string()

    });
    Joi.validate(data, schema, (err, value) => {

        if (err) {
            res.status(422).json({
                status: 'error',
                message: 'Invalid request data'
            });
        } else {

            res.json({
                status: 'success',
                message: 'User created successfully',
                data: value
            });
        }

    });

});
app.listen(3000,()=>console.log("server is running on port 3000"));

