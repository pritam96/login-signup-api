const mongoose = require('mongoose'); 

mongoose.connect('mongodb://localhost/demo1'); 
mongoose.connection
.once('open', () => console.log('Connection established'))
.on('error', (error) => {
    console.log('Warning : ' + error); 
});
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name:String,
    email: String,
    password:String   
});

/* UserSchema.methods.joiValidate = function(obj) {
	var Joi = require('joi');
	var schema = {
		name: Joi.types.String().min(6).max(30).required(),
		password: Joi.types.String().min(8).max(30).regex(/[a-zA-Z0-9]{3,30}/).required(),
		email: Joi.types.String().email().required()
	}
	return Joi.validate(obj, schema);
} */
UserSchema.methods.joiValidate = function() {
    // pull out just the properties that has to be checked (generated fields from mongoose we ignore)
    const { name, email, password } = this;
    const user = { name, email, password };
    const joi = require("joi");
    const schema = joi.object().keys({
      name: joi.string()
        .min(6)
        .max(24)
        .required(),
      email: joi.string()
        .email()
        .required(),
      password: joi.string()
        .min(8)
        .max(30)
        .regex(/[a-zA-Z0-9]{3,30}/)
        .required(),
      _id: joi.string()
    });
  
    return joi.validate(user, schema, { abortEarly: false });
  };
var USER= mongoose.model('user', UserSchema);
module.exports=USER;
